/**
 * @file main.cpp
 * @brief Main application code for the OpenVenti project
 * @version 0.1
 * @date 2020-04-02
 * 
 * @copyright Copyright OpenVenti project (c) 2020
 *            Except for Files StepperMotor.cpp and StepperMotor.h 
 *            these files belong to their own owners.
 * 
 */

#define INIT_FUNC 1
/* --- Standard Includes --- */
#include <stdio.h>
#include <string.h>

/* --- Project Includes --- */
#include <shunyaInterfaces.h>
#include <functions.h>
#include <influxdb.h>
#include <aws.h>
#include "StepperMotor.h"
#include "HX711.h"

#define CLOCKwISE 1
#define ANTICLOCKWISE -1
#define HOLDTIME 100 // 100 ms needs to be acccording to the MIT specs
#define TOPIC "sensor/temp"

#define HOSPITAL_NAME "test"

// HX711 circuit wiring
#define LOADCELL_DOUT_PIN 2
#define LOADCELL_SCK_PIN 3

#define       PING              1
#define       SET_BPM           2
#define       SET_VOLUME        3
#define       SET_IE            4
#define       READ_DEVID        5
#define       READ_HG           6
#define       READ_IE           7
#define       READ_VOLUME       8
#define       READ_PEEP         9

StepperMotor sm;

HX711 temp;

/* For now the speed is full but need to change the speed according to the BPM
 * Claculations of converting the BPM to speed is pending. 
 * We need to make a formula for this conversion.
 */
int motorSpeed = 100;

/* Degrees to turn the motor, starts from zero
 * for now set to 90 but will depend on he volume 
 * set by the doctors.
 * This parameter also needs formula and calculation
 */
int degrees = 90;

int bpm = 8;
int ie = 4;
int hg = 20;
int peep = 40;
int volume = 10;
int pin = 40;
long lastUpdateTime = 0;
struct AWSSettings set = {
        "a3rh361ag55y5n-ats.iot.us-east-2.amazonaws.com",/* MQTT HOST URL */
        8883,/* MQTT port */
        "./modules/cloud/certs", /* Cert location */
        "AmazonRootCA1.pem", /* Client Root CA file name */
        "187a480729-certificate.pem.crt", /* Client certificate file name */
        "187a480729-private.pem.key", /* Client Private Key file name */
        1, /* Quality Of Service 0 or 1 */
        "test-3-25" /* Client ID */
};

struct InfluxdbSettings InfluxSet = { 
        "http://148.251.91.243:8086", 	/* Database URL */
        "venti_test" 			/* Database Name */
};

int setMotorSpeedFromBPM(int bpm)
{
        if (bpm > 40) {
                return -101;
        } else if (bpm < 8) {
                return -102;
        } else {
                motorSpeed = 100;
        }
        return 0;
}

int setDegFromVolume(int vol)
{
        if (vol > 100) {
                return -101;
        } else if (vol < 0) {
                return -102;
        } else {
                degrees = 90;
        }
        return 0;
}

void remoteControl(int topicNameLen, char *topicName, int msgLen, char *msg) 
{
        char payload[1024];
        /**
         * The command from AWS remote is present in the msg variable
         * We compare the command to the actual commands and take action
         * accordingly.
         */
        int cmd = 0;
        if(strncmp(msg, "set bpm", 7) == 0 ) {
                cmd = SET_BPM;
        } else if(strncmp(msg, "set volume", 10) == 0 ) {
                cmd = SET_VOLUME;  
        } else if(strncmp(msg, "set ie", 6) == 0 ) {
                cmd = SET_VOLUME;  
        } else if(strncmp(msg, "read devid", 10) == 0 ) {
                cmd = READ_DEVID;  
        } else if(strncmp(msg, "read hg", 7) == 0 ) {
                cmd = READ_HG;  
        } else if(strncmp(msg, "read ie", 7) == 0 ) {
                cmd = READ_IE;  
        } else if(strncmp(msg, "read volume", 11) == 0 ) {
                cmd = READ_VOLUME;  
        } else if(strncmp(msg, "read peep", 9) == 0 ) {
                cmd = READ_PEEP;  
        } else if(strncmp(msg, "ping", 4) == 0 ) {
                cmd = PING;
        }
        switch (cmd) {
                case PING:
                        printf("Ping by AWS remote.\n");
                        aws_iot_pub (set, TOPIC, strlen(TOPIC), "Ping received by device, Ping Back.", strlen("Ping received by device, Ping Back."));  
                        break;
                case SET_BPM:{
                        char temp1[10], temp2[10];
                        int bpm;
                        sscanf(msg, "%s %s %d", &temp1, &temp2, &bpm );
                        if (setMotorSpeedFromBPM(bpm) < 0){
                                aws_iot_pub (set, TOPIC, strlen(TOPIC), "Incorrect BPM value, Unable to set BPM.", strlen("Incorrect BPM value, Unable to set BPM."));
                        }

                        break;
                }
                case SET_VOLUME: {                   
                        printf("Triggered Volume setting by AWS remote.\n");
                        char temp1[10], temp2[10];
                        int vol;
                        sscanf(msg, "%s %s %d", &temp1, &temp2, &vol );
                        if (setDegFromVolume(vol) < 0){
                                aws_iot_pub (set, TOPIC, strlen(TOPIC), "Incorrect Volume, Unable to set Volume.", strlen("Incorrect Volume, Unable to set Volume."));
                        }
                        break;
                }
                case SET_IE: 
                        printf("Triggered IE setting by AWS remote.\n");
                        aws_iot_pub (set, TOPIC, strlen(TOPIC), "Incorrect IE, Unable to set IE.", strlen("Incorrect IE, Unable to set IE."));  
                        break;
                case READ_DEVID: 
                        /*Debug Print */
                        printf("Triggered Read ID setting by AWS remote.\n");
                        /* Write the payload to be sent */
                        sprintf(payload, "{ \"id\": %s }", DEVICE_ID);
                        /* Send to the AWS cloud */
                        aws_iot_pub (set, TOPIC, strlen(TOPIC), payload, strlen(payload));  
                        
                        break;
                case READ_IE: 
                        /*Debug Print */
                        printf("Triggered Read IE setting by AWS remote.\n");
                        /* Write the payload to be sent */
                        sprintf(payload, "{ \"ie\": 1:%d }", ie);
                        /* Send to the AWS cloud */
                        aws_iot_pub (set, TOPIC, strlen(TOPIC), payload, strlen(payload));  
                        break;
                case READ_HG: 
                        /*Debug Print */
                        printf("Triggered Read HG setting by AWS remote.\n");
                        /* Write the payload to be sent */
                        sprintf(payload, "{ \"hg\": 1:%d }", hg);
                        /* Send to the AWS cloud */
                        aws_iot_pub (set, TOPIC, strlen(TOPIC), payload, strlen(payload));  
                        break;
                case READ_VOLUME: 
                        /*Debug Print */
                        printf("Triggered Read Volume setting by AWS remote.\n");
                        /* Write the payload to be sent */
                        sprintf(payload, "{ \"volume\": %d }", volume);
                        /* Send to the AWS cloud */
                        aws_iot_pub (set, TOPIC, strlen(TOPIC), payload, strlen(payload));  
                        break;
                case READ_PEEP: 
                        /*Debug Print */
                        printf("Triggered Read PEEP setting by AWS remote.\n");
                        /* Write the payload to be sent */
                        sprintf(payload, "{ \"peep\": %d }", peep);
                        /* Send to the AWS cloud */
                        aws_iot_pub (set, TOPIC, strlen(TOPIC), payload, strlen(payload));  
                        break;
                default : return ;
        }

}

void awsControlInit(void)
{
	aws_iot_sub (set, TOPIC, strlen(TOPIC), remoteControl);
}



void loop(void) 
{
        /* =================================================== */
        /* TODO: Make sure the code below goes to another thread */

        /* NOTE: Before starting, the current position of the
        * stepper motor corresponds to 0 degrees
        */
        // Rotate of 90 degrees clockwise at 100% of speed
        sm.run(CLOCKwISE, degrees, motorSpeed);
        sm.wait(HOLDTIME);
        // Rotate of 90 degrees counterclockwise at 80% of speed
        sm.run(ANTICLOCKWISE, degrees, motorSpeed);
        /** Check Homing switch and reset to zero if not active
         */
        if(digitalRead(pin) == LOW) {
                sm.resetCurrPos();
        }
        sm.wait(HOLDTIME);

        long bTemp;
        /*Read the body temperature from the sensor */
        if (temp.is_ready()) {
                bTemp = temp.read();
                cout<<bTemp;
        }

        /* =================================================== */
        /* Send data to cloud every 30 secs*/
	long currTime = micros();
        if (currTime - lastUpdateTime > 30000) {
                writeDbInflux(InfluxSet, "venti1,hospital=%s bpm=%d,volume=%d,temp=%ld" ,HOSPITAL_NAME, bpm, volume, bTemp);
		lastUpdateTime = currTime;
        }




}

void setup(void) 
{
	initLib();
        // StepperMotor object declaration
        sm.setGPIOutputs(37, 35, 33, 31);
        temp.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
	awsControlInit();
}

int main (void) 
{
        setup();
	while(1) {
                loop();
	}
        return 0;
}