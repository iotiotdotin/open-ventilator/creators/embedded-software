#include "HX711.h"

// HX711 circuit wiring
const int LOADCELL_DOUT_PIN = 2;
const int LOADCELL_SCK_PIN = 3;

HX711 scale;


int main () {
  //Serial.begin(57600);
  scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);

  if (scale.is_ready()) {
    long reading = scale.read();
    cout<<"HX711 reading: ";
   cout<<reading;
  } else {
    cout<<"HX711 not found.";
  }

  //delay(1000);

}